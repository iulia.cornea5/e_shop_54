package org.example.repository;

import org.example.entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class SupplierRepository {

    private final SessionFactory sessionFactory;

    public SupplierRepository(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    // salvează un supplier în baza de date
    public void save(Supplier supplier) {
        Session session = sessionFactory.openSession();         // deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();   // începe o tranzacție

        session.persist(supplier);                              // avem o singură modificare de făcut, și anume: salvarea supplier-ului

        transaction.commit();                                   // salveaza în baza de date modificarile facute după deschiderea tranzacție
        session.close();                                        // închide sesiunea de comunicare cu baza de date
    }


}
