package org.example.repository;

import org.example.entity.Client;
import org.example.entity.Product;
import org.example.entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ProductRepository {

    private final SessionFactory sessionFactory;

    public ProductRepository(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    // salvează un supplier în baza de date
    public void save(Product product) {
        Session session = sessionFactory.openSession();         // deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();   // începe o tranzacție

        session.persist(product);                              // avem o singură modificare de făcut, și anume: salvarea product-ului

        transaction.commit();                                   // salveaza în baza de date modificarile facute după deschiderea tranzacție


        Transaction t1 = session.beginTransaction();
        session.persist(new Client(10, "ana", "maria", "email@.com"));
        t1.commit();

        Transaction t2 = session.beginTransaction();
        // add 100 RON to Ionel
        // substract 100 RON from Maria
        t2.commit();

        session.close();                                        // închide sesiunea de comunicare cu baza de date
    }

    public List<Product> getAll() {
        List<Product> products;
        Session session = sessionFactory.openSession();
        products = session.createQuery("SELECT p FROM Product p", Product.class).getResultList(); // acesta este un query HQL nu SQL
        session.close();
        return products;
    }
}
