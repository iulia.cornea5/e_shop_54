package org.example.repository;

import org.example.entity.Client;
import org.example.entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ClientRepository {

    private final SessionFactory sessionFactory;

    public ClientRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    // salvează un client în baza de date
    public void save(Client client) {
        Session session = sessionFactory.openSession();         // deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();   // începe o tranzacție

        session.persist(client);                                // avem o singură modificare de făcut, și anume: salvarea client-ului

        transaction.commit();                                   // salveaza în baza de date modificarile facute după deschiderea tranzacție
        session.close();                                        // închide sesiunea de comunicare cu baza de date
    }

    public List<Client> getAll() {
        List<Client> clients = new ArrayList<>();
        Session session = sessionFactory.openSession();
        // sql:   SELECT * FROM client;
        // acesta este un query de hibernate (e ușor diferit de query-ul din SQL)
        // hql:   SELECT c FROM Client c
        // selecteaza toate rândurile din tabela Client
        // "c" este doar un alias pentru un rând
        clients = session.createQuery("SELECT c FROM Client c", Client.class).getResultList();
        session.close();
        return clients;
    }

}
