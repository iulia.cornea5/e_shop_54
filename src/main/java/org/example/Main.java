package org.example;


import org.example.config.DatabaseConfig;
import org.example.entity.Client;
import org.example.entity.Product;
import org.example.entity.ProductType;
import org.example.entity.Supplier;
import org.example.repository.ClientRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SupplierRepository;

import java.time.LocalDate;
import java.util.List;

public class Main {

    public static SupplierRepository supplierRepository =
            new SupplierRepository(DatabaseConfig.getSessionFactory());

    public static ClientRepository clientRepository =
            new ClientRepository(DatabaseConfig.getSessionFactory());

    public static ProductRepository productRepository =
            new ProductRepository((DatabaseConfig.getSessionFactory()));

    public static void main(String[] args) {
        Supplier s1 = new Supplier(1, "Pepiniera Mărioara", "Brașov - România");
        Supplier s2 = new Supplier(2, "Depozitul de lalele", "Olanda");
        supplierRepository.save(s1);
        supplierRepository.save(s2);

        Client c1 = new Client(1, "Ion", "Pop", "ion.pop@gmail.com");
        Client c2 = new Client(2, "Ioana", "Pop", "ioana.pop@gmail.com");
        Client c3 = new Client(3, "Ionuț", "Pop", "ionut.pop@gmail.com");
        clientRepository.save(c1);
        clientRepository.save(c2);
        clientRepository.save(c3);

        Product p1 = new Product(1, ProductType.NATURAL_FLOWERS,
                "Trandafiri", "Rosii",
                11, 280.00, 300.00,
                LocalDate.of(2023, 6, 1),
                LocalDate.of(2023, 6, 30));
        productRepository.save(p1);

        displayAllClients();
        displayAllProducts();

    }

    private static void displayAllProducts() {
        List<Product> products = productRepository.getAll();
        for (Product p : products) {
            System.out.println(p.getName() + " " +
                            p.getDescription() + " " +
                            p.getQuantity() + " bucăți " +
                            " cumpărați la " + p.getBuyingDate() +
                            " cu prețul de " + p.getBuyingPrice() + "RON " +
                            " valabili până la " + p.getExpirationDate() +
                            " disponibili la prețul de " + p.getSellingPrice() + "RON."
            );
        }

    }

    public static void displayAllClients() {
        List<Client> clients = clientRepository.getAll();
        for (Client c : clients) {
            System.out.println("Nume: " + c.getLastName() + " Prenume: " + c.getFirstName() + " Email: " + c.getEmail());
        }
    }


}