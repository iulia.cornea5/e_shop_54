package org.example.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_feedback")  // atenție!!!! @Table <- e pentru redenumirea tabelului și @Entity e pentru crearea tabelului
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserFeedback {

    @Id
    private Integer id;

    @Enumerated(value = EnumType.STRING) // se aasigură că în baza de date se salvează cuvântul GOOD sau BAD și nu cifrele 1 sau 2
    private Rating rating;

    @Column(name = "feedback_description")  // atenție @Column nu crează coloana în sine (@Entity se ocupă de crearea tabelului cu coloanele lui)
    private String feedbackDescription;     // @Column doar redenumește coloana din feedbackDescription în feedback_description


    public String toString() {
        String s = "User feedback " + id + " este " + rating + ": " + feedbackDescription;
        return s;
    }
}
